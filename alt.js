function initPixi() {
    var renderer = PIXI.autoDetectRenderer(window.innerWidth - 20, window.innerHeight - 20);
    document.body.appendChild(renderer.view);
    var stage = new PIXI.Container();
    stage.interactive = true;
    stage.buttonMode = true;
    stage.hitArea = new PIXI.Rectangle(0,0,window.innerWidth -20,window.innerHeight-20);

    animate();

    function animate()
    {
        requestAnimationFrame(animate);
        renderer.render(stage, false);
    }

    var amebaTexture = PIXI.Texture.fromImage('./ameba.png');
    var zooplanktonTexture = PIXI.Texture.fromImage('./zooplankton.png');

    var zooplankton = new PIXI.Sprite(zooplanktonTexture);
    zooplankton.props = {
        name: "Zooplankton",
        foodChainPosition: 1,
        mov: 3,
        mainStatus: "hungry",
        subStatus: "",
        ac: 1,
        maxhp: 2,
        hp: 1
    };
    zooplankton.position.x = 300;
    zooplankton.position.y = 300;
    zooplankton.scale.x = 0.75;
    zooplankton.scale.y = 0.75;
    stage.addChild(zooplankton);
    var ticker = new PIXI.ticker.Ticker();
    zooplankton.ticker = ticker;
    zooplankton.ticker.stop();
    zooplankton.ticker.add(function (deltaTime) {
        breathOfLife(zooplankton);

    });
    zooplankton.ticker.start();


    function createAmeba(e, x, y) {
        var a = new PIXI.Sprite(amebaTexture);
        a.props = {
            name: "Ameba",
            foodChainPosition: 0,
            mov: 1,
            mainStatus: "idle",
            ac: 0,
            maxhp: 1,
            hp: 1
        };
        a.interactive = true;
        a.anchor.set(0.5);
        a.buttonMode = true;
        a.on('pointerdown', meiosis);
        a.position.x = e ? e.data.global.x : x +20;
        a.position.y = e? e.data.global.y : y;
        stage.addChild(a);

        var ticker = new PIXI.ticker.Ticker();
        a.ticker = ticker;
        a.ticker.stop();
        a.ticker.add(function (deltaTime) {
            breathOfLife(a);
        });
        a.ticker.start();
    }

    stage.interactive = true;
    stage.on('pointerdown', createAmeba);

    function meiosis(e) {
        createAmeba(null, this.position.x, this.position.y);
        e.stopPropagation();
    }

    function checkCollisions(obj)
    {
        var collidedObjects = [];
        _.each(stage.children, function(child){
            var ab = obj.getBounds();
            var bb = child.getBounds();
            if (ab.x + ab.width > bb.x && ab.x < bb.x + bb.width && ab.y + ab.height > bb.y && ab.y < bb.y + bb.height) {
                collidedObjects.push(child);
            };
        })
        return collidedObjects;
    }

    function findNearestFood(being) {
        var foundFood = [];
        _.each(stage.children, function (child) {
            if (child.props.foodChainPosition < being.props.foodChainPosition) {
                foundFood.push(child);
            }
        });

        return foundFood;
    }

    function breathOfLife(being) {
        //Move based on statuses
        switch (being.props.mainStatus) {
            case "idle": {
                being.position.x += Math.round(Math.random() * (being.props.mov - being.props.mov*-1) + being.props.mov*-1);
                being.position.y += Math.round(Math.random() * (being.props.mov - being.props.mov*-1) + being.props.mov*-1);
                break;
            }

            case "hungry": {
                if (being.props.hunting) {
                    var modifier = being.props.mov + 2;
                    being.rotation = Math.atan2(being.props.hunting.y - being.position.y, being.props.hunting.x - being.position.x);
                    being.position.x += Math.cos(being.rotation) * modifier;
                    being.position.y += Math.sin(being.rotation) * modifier;
                }
                else {
                    var foodFound = findNearestFood(being);
                    being.props.hunting = foodFound[0];
                    being.position.x += Math.round(Math.random() * (being.props.mov - being.props.mov * -1) + being.props.mov * -1);
                    being.position.y += Math.round(Math.random() * (being.props.mov - being.props.mov * -1) + being.props.mov * -1);
                }
                break;
            }
        }

        //Check collisions
        var collidedObjs = checkCollisions(being)
        _.each(collidedObjs, function(obj){
            if (obj.props.foodChainPosition < being.props.foodChainPosition) {
                stage.removeChild(obj);
                being.props.hunting = null;
            }
        })

        //Update statuses


    }
}
