function initPixi() {
    var app = new PIXI.Application(window.innerWidth -20, window.innerHeight-20, {backgroundColor: 0x000000});
    document.body.appendChild(app.view);
    var stage = new PIXI.Container();
    app.stage.interactive = true;
    app.stage.buttonMode = true;
    app.stage.hitArea = new PIXI.Rectangle(0,0,window.innerWidth -20,window.innerHeight-20);
    app.stage.on('pointerdown', createAmeba);

    var ameba = {
        name: "Ameba",
        mov: 1,
        tmov: "simples",
        ac: 0,
        maxhp: 1
    };

    var zooplankton = {
        name: "Zooplankton",
        mov: 3,
        tmov: "simples",
        ac: 1,
        maxhp: 2
    };

    var amebaTexture = PIXI.Texture.fromImage('./ameba.png');
    var zooplanktonTexture = PIXI.Texture.fromImage('./zooplankton.png');

    var a1 = JSON.parse(JSON.stringify(ameba));
    a1.name += "_A";
    a1.sprite = new PIXI.Sprite(amebaTexture);
    a1.sprite.anchor.set(0.5);
    a1.sprite.interactive = true;
    a1.sprite.buttonMode = true;
    a1.sprite.on('pointerdown', meiosis);

    var zp = JSON.parse(JSON.stringify(zooplankton));
    zp.sprite = new PIXI.Sprite(zooplanktonTexture);
    zp.sprite.anchor.set(0.5);
    zp.sprite.interactive = true;
    zp.sprite.buttonMode = true;
    zp.sprite.on('pointerdown', poke);

    var tudo = [];
    tudo.push(a1);
    tudo.push(zp);

    a1.posx = app.screen.width / 2;
    a1.posy = app.screen.height / 2;

    zp.posx = app.screen.width - 100;
    zp.posy = app.screen.height - 100;

    app.stage.addChild(a1.sprite);
    app.stage.addChild(zp.sprite);

    app.ticker.add(function (delta) {
        for(i=0;i<tudo.length;i++){
            var beign = tudo[i];

            switch (beign.tmov) {
                case "simples": {
                    var newx = randNum(beign.mov*-1, beign.mov);
                    var newy = randNum(beign.mov*-1, beign.mov);

                    beign.posx = beign.posx + newx >= app.screen.width ? beign.posx : beign.posx + newx;
                    beign.posy = beign.posy + newy >= app.screen.height ? beign.posy : beign.posy + newy;
                }
            }

            beign.sprite.x = beign.posx;
            beign.sprite.y = beign.posy;
        }
    });

    function createAmeba(e, x, y) {
        var a = JSON.parse(JSON.stringify(ameba));
        a.sprite = new PIXI.Sprite(amebaTexture);
        a.sprite.anchor.set(0.5);
        a.sprite.interactive = true;
        a.sprite.buttonMode = true;
        a.sprite.on('pointerdown', meiosis);
        a.posx = e ? e.data.global.x : x +20;
        a.posy = e? e.data.global.y : y;
        app.stage.addChild(a.sprite);
        tudo.push(a);
    }

    function meiosis(e) {
        createAmeba(null, this.position.x, this.position.y);
        e.stopPropagation();
    }

    function poke(e) {
        this.position.x += 100;
        this.position.y += 100;
        e.stopPropagation();
    }

    start();
}

function randNum( min, max ) {
    return Math.round(Math.random() * ( max - min ) + min);
}

function start()
{
    requestAnimationFrame(animate);
    renderer.render(stage, false);
}

